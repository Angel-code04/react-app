import { Container, Card, Modal, Form, Button } from 'react-bootstrap'
import { useState, useEffect } from 'react'

export default function Profile(){

    const [firstName1, setFirstName1] = useState("");
    const [lastName1, setLastName1] = useState("");
    const [userName1, setUserName1] = useState("");
    const [password1, setPassword1] = useState("");
    const [mobileNo1, setMobileNo1] = useState("");

    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [userName, setUserName] = useState("");
    const [password, setPassword] = useState("");
    const [mobileNo, setMobileNo] = useState("");
    const [isActive, setIsActive] = useState(false);

    const [infos, setInfos] = useState([]);
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const user = () => {
        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            setFirstName1(data.firstName);
            setLastName1(data.lastName);
            setUserName1(data.userName);
            setPassword1(data.password);
            setMobileNo1(data.mobileNo);
        })
    }

    useEffect(() => {
        user()
    }, [])

    useEffect(() => {
        if(firstName !== "" && lastName !== "" && userName !== "" && password !== "" && mobileNo !== ""){
            setIsActive(true);
        }

    }, [firstName, lastName, userName, password, mobileNo])

    const update = () => {
        handleShow()
        fetch(`${process.env.REACT_APP_API_URL}/users/info`, {
            method: "PUT",
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                firstName: firstName,
                lastName: lastName,
                userName: userName,
                password: password,
                mobileNo: mobileNo
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)
        })
    }


    return (
        <>
                    <Container>
                        <Container className='mt-3 profile-pic col-lg-3 col-md-3 col-sm-5'>
                            <img className='' width='100%' height='100%' src="https://cdn-icons-png.flaticon.com/512/1053/1053244.png" />
                        </Container>
                    </Container>
                    <Container className='col-lg-10'>
                        <Card className='mt-3 mb-4 border-dark'>
                            <Card.Header as="h4">Profile details</Card.Header>
                            <Card.Body>

                                <Container>
                                    <h5>First Name: <span className='ms-1 span-text'>{firstName1}</span></h5>
                                    <h5>Last Name: <span className='ms-1 span-text'>{lastName1}</span></h5>
                                    <h5>Username: <span className='ms-1 span-text'>{userName1}</span></h5>
                                    <h5>Mobile No. : <span className='ms-1 span-text'>{mobileNo1}</span></h5>
                                </Container>

                              <div className='d-flex'>
                                <Button className='ms-auto' variant="light" onClick={() => handleShow()}>Update</Button>
                              </div>
                            </Card.Body>
                        </Card>
                    </Container>

        <Modal dialogClassName='my-modal' show={show} onHide={handleClose}>
            <Modal.Header closeButton>
              <Modal.Title>Edit details</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                
            <Form onSubmit = {(e) => update(e)} className="my-2">
                <Form.Group className="mb-2" controlId="firstName">
                    <Form.Label>First name</Form.Label>
                    <Form.Control type="text"  value={firstName} placeholder={firstName1} onChange={e => setFirstName(e.target.value)} />
                </Form.Group>

                <Form.Group className="mb-4" controlId="lastName">
                    <Form.Label>Last name</Form.Label>
                    <Form.Control type="text"  value={lastName} placeholder={lastName1} onChange={e => setLastName(e.target.value)} />
                </Form.Group>

                <Form.Group className="mb-4" controlId="userName">
                    <Form.Label>Username</Form.Label>
                    <Form.Control type="text"  value={userName} placeholder={userName1} onChange={e => setUserName(e.target.value)} />
                </Form.Group>

                <Form.Group className="mb-4" controlId="password">
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password"  value={password} onChange={e => setPassword(e.target.value)} />
                </Form.Group>

                <Form.Group className="mb-4" controlId="mobileNo">
                    <Form.Label>Mobile No.</Form.Label>
                    <Form.Control type="text"  value={mobileNo} onChange={e => setMobileNo(e.target.value)} />
                </Form.Group>

                <div className='d-grid gap-2 secondary'>
                {
                    isActive
                    ?
                    <Button variant='dark' type="submit" id="loginBtn" size='lg'>
                    Edit
                    </Button>
                    :
                    <Button variant='dark' type="submit" id="loginBtn" size='lg' disabled>
                    Edit
                    </Button>
                }

                

                </div>
            </Form>


            </Modal.Body>
            <Modal.Footer>
              <Button variant="secondary" onClick={handleClose}>
                Close
              </Button>
            </Modal.Footer>
        </Modal>
        </>
    )
}