import { Navigate} from 'react-router-dom';
import {useContext, useEffect} from 'react';
import userContext from '../UserContext';

export default function Logout(){
    
    // Consume the UserContext object and destructure it to access the 
    const {setUser, unsetUser} = useContext(userContext);

    // Clear the localStorage of the user's information
    unsetUser();
    // console.log(user);

    useEffect(() => {
        // Set the userState back to it's original value.
        setUser({
            id: null,
            isAdmin: null
        })
    })


    return(
        // Redirect back to the login
        <Navigate to="/login" />
    )
};