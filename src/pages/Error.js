import Banner from "../components/Banner";
// import 'bootswatch/dist/vapor/bootstrap.min.css';

export default function Error(){
    const data = {
        title: "404 - Page not fount",
        content: "The page you are looking for cannot be found",
        destination: "/",
        label: "Back home"
    }

    return(
        <Banner data={data} />
    )
};