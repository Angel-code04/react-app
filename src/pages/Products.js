// import coursesData from "../data/coursesData";
import ProductCard from '../components/ProductCard';
import { useEffect, useState, useContext } from 'react';
import UserContext from '../UserContext';
import { Navigate } from 'react-router-dom'
import { Container } from 'react-bootstrap';

export default function Products(){
    
    const {user} = useContext(UserContext);
    const [items, setItems] = useState([]);


    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products`)
    
    .then(res => res.json())
    .then(data => {
        // console.log(data);

        setItems(data.map(product => {
            return (
                <ProductCard key={product._id} itemProp={product} />
                )
            }))
        })
    }, [])

    
    return(
        (user.isAdmin)
        ?
            <Navigate to='/admin' />
        :
        <>
            <Container>
            <h1 className='ms-3 my-3'>Items</h1>
            </Container>
            <Container className='col-lg-9 col-md-12 col-sm-12'>
            {items}
            </Container>
        </>
    );
}

