import { useEffect, useState, useContext} from 'react';
import { Navigate, Link } from 'react-router-dom'
import { Container, Nav, Table } from 'react-bootstrap';
import UserContext from '../UserContext';

export default function Orders(){

    const {user} = useContext(UserContext);

    const [orders, setOrders] = useState([]);
    const [total , setTotal] = useState(0);

    const [haveOrder, setHaveOrder] = useState(false)

    const fetchOrders = () => {
        fetch(`${process.env.REACT_APP_API_URL}/users/getUserOrders`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            if(data.length === 0){
                setHaveOrder(false)
            }
            else{
                setHaveOrder(true);
                setOrders(data.map((item) => {
                    return (
                        <tr key={item._id}>
                            <td>{item.orderedOn}</td>
                            <td>{item.name}</td>
                            <td>{item.quantity}</td>
                            <td>{item.status}</td>
                        </tr>
                    )
                }))
            }
        })
    }
        
    useEffect(() => {
        fetchOrders()
    }, [])


    return(
        (user.id == null)
        ?
            <>
                <Container>
                    <h1 className='d-flex justify-content-center align-items-center my-3'>Orders</h1>
                    <p className='d-flex justify-content-center align-items-center my-3'><Nav.Link as={Link} to='/register' className='d-inline-block text-success px-1'>Create an account</Nav.Link> first or <Nav.Link as={Link} to='/login' className='d-inline-block text-success px-1'>log in</Nav.Link> to your account</p>
                </Container>
            </>
        :
        <>
            {
                (haveOrder)
                ?
                <>
                <Container className='justify-content-center align-items-center d-flex mt-3'>
                <h1>Orders</h1>
                </Container>
                <Container className='mt-3 text-center'>
                <Table striped bordered hover>
                        <thead>
                          <tr>
                            <th>Ordered On</th>
                            <th>Product Name</th>
                            <th>Quantity</th>
                            <th>Status</th>
                          </tr>
                        </thead>
                        <tbody>
                          {orders}
                        </tbody>
                    </Table>
                </Container>
                </>

                :
                <Container className='text-center mt-3'>
                <h1 className='d-flex justify-content-center align-items-center my-3'>Orders</h1>
                        <p>Nothing on you order yet. Go to <Nav.Link as={Link} to='/products' className='d-inline-block text-success px-1'>Products</Nav.Link> to view our available items</p>
                </Container>
                
            }
        </>
    )
}