import Banner from '../components/Banner';
import { useContext, useEffect, useState } from 'react';
import { Link } from 'react-router-dom'
import UserContext from '../UserContext';
import { Container, Row, Col, Card, Button } from 'react-bootstrap'


export default function Home(){

    const {user} = useContext(UserContext);
    const [saleItems, setSaleItems] = useState([]);

    const sale = () => {
        fetch(`${process.env.REACT_APP_API_URL}/products/sale`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            setSaleItems(data.map(item => {
                return (
                    
                    
                    <Container key={item._id} className='col-lg-4 col-md-6 col-sm-12 d-inline-block text-center' data-aos="zoom-in-down" >
                    
    
                    <div className="flip-card">
                        <div className="flip-card-inner">
                          <div className="flip-card-front">
                          <Row>
                            <Col>
                                <Card className="border-secondary cardHighlight ">
                                <Card.Body className="homeCard">
                                <Card.Title className="card-header mb-2 cardTitle text-card">
                                    {item.name}
                                </Card.Title>
        
                                <Card.Subtitle>
                                    Price:
                                </Card.Subtitle>
        
                                <Card.Text>
                                    {item.price}
                                </Card.Text>
        
                            </Card.Body>
                                </Card>
        
        
                            </Col>
                        </Row>
                          </div>
                          <div key={item._id}  className="flip-card-back">
                          <Row>
                            <Col>
                                <Card className="bg-secondary
                                 mb-3 cardHighlight">
                                <Card.Body className='text-white'>
        
                                <Card.Subtitle>
                                    Description:
                                </Card.Subtitle>
                                <Card.Text>
                                    {item.description}
                                </Card.Text>
        
                                <Button  as={Link} to={`/products/${item._id}`} variant="info" className='mt-4'>Details</Button>
                            </Card.Body>
                                </Card>
        
        
                            </Col>
                        </Row>
                          </div>
                        </div>
                    </div>
        
                    </Container>
                    )
                }))
        })

    }

    useEffect(() => {
        sale()
    }, [])

    return(
        (user.isAdmin)
        ?
            <Banner />
        :
        <>
            <Banner />
            <Container>
                
            {saleItems}
            </Container>

        </>
    )
}