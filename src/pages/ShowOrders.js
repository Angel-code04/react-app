import { Accordion, Container, Button, Modal } from "react-bootstrap"
import { useState, useEffect } from 'react';
import Swal from 'sweetalert2';

export default function ShowOrders(){

    const [accordionHeader, setAccordionHeader] = useState([]);
    const [userOrders, setUserOrders] = useState([])

    const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

    
    const fetchUsers = () => {
        fetch(`${process.env.REACT_APP_API_URL}/users/allUsers`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }

        })
        .then(res => res.json())
        .then(data => {
            setAccordionHeader(data.map(user =>{
                // console.log(user)
                return (

                    <Accordion.Item key={user._id} eventKey={user._id} >
                        <Accordion.Header>{user.firstName} {user.lastName}</Accordion.Header>
                        <Accordion.Body>
                            <div className="ms-5 mt-2">
                                <h5>Username:  <span className="span"> {user.userName}</span></h5>
                                

                                <h5 >Mobile No. : <span className="span"> {user.mobileNo}</span></h5>
                                

                                <h5 >Email: <span className="span"> {user.email}</span></h5>

                                <h5 >isAdmin: <span className="span"> {user.isAdmin ? "true" : "false"}</span></h5>
                                
                            </div>
                            {
                                (user.isAdmin)
                                ?
                                <div className='d-flex'>
                                    <Button className="ms-auto btn-dark" size='sm' onClick={() => change(user._id)}>Change Status</Button>
                                </div>
                                :
                                <div className='d-flex'>
                                    <Button className='ms-auto btn-info' size='sm' onClick={() => orders(user._id)}>View Orders</Button>

                                    <Button className='ms-1 btn-dark' size='sm' onClick={() => change(user._id)}>Change Status</Button>
                                </div>
                            }
                        </Accordion.Body>
                    </Accordion.Item>
                )
            }))
        })
    }

    const change = (userId) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/${userId}/UpdateStatusAdmin`, {
            method: "PATCH",
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {console.log(data)})
    }

    const orders = (userId) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/${userId}/viewOrders`, {
                    headers: {
                        Authorization: `Bearer ${localStorage.getItem('token')}`
                    }
                })
                .then(res => res.json())
                .then(data  => {
                    handleShow()
                    // console.log(data)


                    if(data.orders.length === 0){
                        return (
                            setUserOrders("No Orders Yet")
                        )
                    }
                    else{
                        setUserOrders(data.orders.map((item) => {
                            // console.log(item)
                            return(

                                <Container key={item._id}>
                                    <h6>Product Id: <span className='ps-3'>{item.productId}</span></h6>
                                    <h6>Product Name: <span className='ps-3'>{item.name}</span></h6>
                                    <h6>Quantity: <span className='ps-3'>{item.quantity}</span></h6>
                                    <h6>Ordered On: <span className='ps-3'>{item.orderedOn}</span></h6>
                                    <h6>Status: <span className='ps-3'>{item.status}</span></h6>
                                    <div className="d-flex">

                                        <Button className="btn-warning ms-auto" size='sm' onClick={() => pending(item._id)}>Pending</Button>
                                        <Button className="btn-success ms-1" size='sm' onClick={() => completed(item._id)}>Completed</Button>
                                        <Button className="btn-danger ms-1" size='sm' onClick={() => removeOrder(item._id)}>Remove</Button>
                                    </div>
                                    <hr />

                                </Container>
                            )
                        }))
                    }

    })

    }


    const pending = (itemId) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/${itemId}/pending`, {
            method: "POST",
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            if(!data){
                Swal.fire({
                    title: "Updated!",
                    icon: "info",
                    text: "You have change the status of an order from Completed to Pending"
                })
            }
            else{
                Swal.fire({
                    title: "Error",
                    icon: "error",
                    text: "An error occur in updating an order"
                })
            }
        })

    }


    const completed = (itemId) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/${itemId}/completed`, {
            method: "POST",
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            if(!data){
                Swal.fire({
                    title: "Updated!",
                    icon: "info",
                    text: "You have changed the status of an order from Pending to Completed"
                })
            }
            else{
                Swal.fire({
                    title: "Error",
                    icon: "error",
                    text: "An error occur in removing an order"
                })
            }
        })

    }


    const removeOrder = (itemId) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/${itemId}/deleteOrder`, {
            method: "DELETE",
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            if(!data){
                Swal.fire({
                    title: "Removed the order",
                    icon: "info",
                    text: "You have removed an order"
                })
            }
            else{
                Swal.fire({
                    title: "Error",
                    icon: "error",
                    text: "An error occur in removing an order"
                })
            }
        })

    }

    useEffect(() => {
        fetchUsers();
    }, [accordionHeader])

    useEffect(() => {
        fetchUsers();
    }, [])

    return(
        <Container className="mt-3">

            <h1>User's Orders</h1>
            <Container>

            <Accordion>
            {/* <Accordion.Item eventKey="">
              
              </Accordion.Item> */}
              {accordionHeader}


                    <Modal show={show} onHide={handleClose}>
                      <Modal.Header closeButton>
                        <Modal.Title>Orders</Modal.Title>
                      </Modal.Header>
                      <Modal.Body>
                        {userOrders}


                      </Modal.Body>
                      <Modal.Footer>
                        <Button variant="secondary" onClick={handleClose}>
                          Close
                        </Button>
                      </Modal.Footer>
                    </Modal>
 
            </Accordion>
            </Container>
        </Container>
    )       
}