import { Carousel, Container, Row, Col, Form } from "react-bootstrap";
import img1 from './img1.jpeg';
import img2 from './img2.jpeg';
import img3 from './img3.jpeg';

export default function Banner(){




    return(
      <>
        {/* <Container>
        <div class="form-check form-switch">
        <Form.Control type="checkbox" id="flexSwitchCheckDefault" />

        <label class="form-check-label" for="flexSwitchCheckDefault">Default switch checkbox input</label>
      </div>
        </Container> */}
        <Container fluid className='mt-3'>
          <Row className='justify-content-center'>
            <Col lg='11' md='12' sm='12'>
            <Carousel fade>
              <Carousel.Item className='carousel-img'>
                <img fluid width='100%' height='100%'
                  className="d-block w-100 roundCarousel"
                  src="https://storage.googleapis.com/gweb-uniblog-publish-prod/original_images/Flagship_100_Blog_2880x1800_Apparel.jpg"
                  alt="First slide"
                  
                />
              </Carousel.Item>

              <Carousel.Item className='carousel-img'>
                <img fluid width='100%' height='100%'
                  className="d-block w-100 roundCarousel"
                  src='https://s.tmimgcdn.com/scr/1200x750/207600/family-online-shopping-free-vector-illustration-concept_207629-original.jpg'
                  alt="Second slide"
                />
              </Carousel.Item>

              <Carousel.Item className='carousel-img'>
                <img fluid width='100%' height='100%'
                  className="d-block w-100 roundCarousel"
                  src='https://img.freepik.com/free-vector/people-standing-holding-shopping-bags_23-2148825441.jpg?w=2000'
                  alt="Third slide"
                />
              </Carousel.Item>

            </Carousel>
            </Col>
          </Row>
        </Container>

        <Container className="d-flex align-items-center justify-content-center text-center">
          <Col xs='12' md='10'  className='my-4'>
              <h1 className="text-align-center">Hot Products</h1>
          </Col>
            
        </Container>

        </>
    )
}