import{useState, useEffect} from "react";
import {Card, Button} from "react-bootstrap";
import { Link } from "react-router-dom";

// Decontruct the "courseProp" from the "props" object to shoerten syntax
export default function ProductCard({itemProp}){

    const {_id, name, description} = itemProp;

    return(
        <Card className="p-3 my-3 mx-5" key={_id}>
            <Card.Body>
                <Card.Title  className="text-card">
                    {name}
                </Card.Title>

                <Card.Subtitle>
                    Description:
                </Card.Subtitle>

                <Card.Text>
                    {description}
                </Card.Text>


                <Button  as={Link} to={`/products/${_id}`} variant="info">Details</Button>
            </Card.Body>
    </Card>
  );
}