import logo from './background.png'

import {Link} from 'react-router-dom';

import { useState, useContext } from 'react';
import Usercontext from '../UserContext';

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCart, faUser, faList, faCartShopping } from "@fortawesome/free-solid-svg-icons";

import {Navbar, Nav, Container, Form, Button} from 'react-bootstrap';
import UserContext from '../UserContext';


export default function AppNavbar(){

    const {user} = useContext(UserContext);

    return(
        <Navbar className="navbar navbar-dark bg-dark navbarPadding" expand='lg'>
        <Container className='navbarScrollHeight'>

            <Navbar.Brand as={Link} to="/" className='d-flex pt-2'>
            <img
              alt=""
              src="/background.png"
              width="30"
              height="30"
              className="d-inline-block"
            />
            <h3 className='text-white ms-1'> Push Card</h3>
            </Navbar.Brand>

            <Navbar.Toggle aria-controls="navbarScroll" />
            <Navbar.Collapse id="navbarScroll" className='navbarScrollLinks'>

                <Nav className="ms-auto my-2 my-lg-0" defaultActiveKey="/" style={{ maxHeight: '100px' }}
                navbarScroll
                >
                    
                    <Nav.Link as={Link} to="/" eventKey="/" className='mx-1'>Home</Nav.Link>
                    
                    {
                        (user.isAdmin)
                        ?
                        <Nav.Link as={Link} to="/admin" eventKey="/admin" className='mx-1'>Admin Dashboard</Nav.Link>
                        :
                        <>
                        <Nav.Link as={Link} to="/products" eventKey="/products" className='mx-1'>Products</Nav.Link>

                        <Nav.Link as={Link} to="/cart" eventKey="/cart" className='mx-1'><FontAwesomeIcon icon={faCartShopping} /> Cart</Nav.Link>

                        <Nav.Link as={Link} to="/orders" eventKey="/orders" className='mx-1'><FontAwesomeIcon icon={faList} /> Orders</Nav.Link>
                        </>
                    }
                    



                    {
                        (user.id !== null)
                        ?
                            <>
                                {
                                    (user.isAdmin)
                                    ?
                                    <></>
                                    :
                                    <Nav.Link as={Link} to="/profile" eventKey="/profile" className='mx-1'><FontAwesomeIcon icon={faUser} /> Profile</Nav.Link>
                                }
                            <Nav.Link as={Link} to="/logout" eventKey="/login" className='mx-1'>Logout</Nav.Link>
                            </>

                        :
                            <>
                                <Nav.Link as={Link} to="/login" eventKey="/login" className='mx-1'>Login</Nav.Link>

                                <Nav.Link as={Link} to="/register" eventKey="/register" className='mx-1'>Register</Nav.Link>
                            </>

                    }
                </Nav>
            </Navbar.Collapse>
        </Container>
    </Navbar>
    )
};